package bg.swift.socialsystem;

import bg.swift.socialsystem.InputMethods.ConsoleInput;
import bg.swift.socialsystem.InterfaceRepos.PersonRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;
import static bg.swift.socialsystem.InformationParser.personCreator;

@Service
public class PersonRegistrationService implements ApplicationRunner {

    private final PersonRepository personRepository;

    public PersonRegistrationService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        ConsoleInput consoleInput = new ConsoleInput();
        int numberOfPeople = consoleInput.initialSizeParser();

        String[] line1;
        String[] line2;
        String[] line3;

        for (int i = 0; i < numberOfPeople; i++) {
            line1 = consoleInput.informationParser();
            line2 = consoleInput.informationParser();
            line3 = consoleInput.informationParser();
            personRepository.save(personCreator(line1, line2, line3));
        }
    }
}
