package bg.swift.socialsystem.Education;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class BachelorEducation extends Education {

    public BachelorEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate,
                             String hasGraduated, String finalGrade) throws Exception {
        super(institutionName, enrollmentDate, graduationDate, hasGraduated, finalGrade);
        setEducationType("Bachelor Education");
    }


    private BachelorEducation() {
    }


}
