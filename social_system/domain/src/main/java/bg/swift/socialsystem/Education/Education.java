package bg.swift.socialsystem.Education;

import bg.swift.socialsystem.Exceptions.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;


@Entity
public abstract class Education {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String institutionName;
    private LocalDate enrollmentDate;
    private LocalDate graduationDate;
    private boolean hasGraduated;
    private Double finalGrade;
    private String educationType;

    protected Education() {

    }

    protected Education(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate, String hasGraduated, String finalGrade) throws Exception {
        setInstitutionName(institutionName);
        setEnrollmentDate(enrollmentDate);
        setGraduationDate(graduationDate);
        setGraduation(hasGraduated);
        setFinalGrade(finalGrade);
    }

    private void setInstitutionName(String institutionName) throws Exception {
        if (institutionName.equals("")) {
            throw new EmptyFieldException();
        }
        this.institutionName = institutionName;
    }

    private void setEnrollmentDate(LocalDate enrollmentDate) throws Exception {
        if (enrollmentDate.isAfter(LocalDate.now())) {
            throw new EnrollmetDateAfterPresentException();
        }
        this.enrollmentDate = enrollmentDate;
    }

    private void setGraduationDate(LocalDate graduationDate) throws Exception {
        if (graduationDate.isBefore(enrollmentDate)) {
            throw new GraduationDateBeforeEnrollmentDateException();
        }
        this.graduationDate = graduationDate;
    }

    private void setGraduation(String hasGraduated) throws Exception {
        if (hasGraduated.toUpperCase().equals("YES")) {
            this.hasGraduated = true;
        } else if (hasGraduated.toUpperCase().equals("NO")) {
            this.hasGraduated = false;
        } else {
            throw new GraduationValueException();
        }
    }

    private void setFinalGrade(String finalGrade) throws Exception {
        Double grade = 0.0;
        if (!finalGrade.equals("")) {
            grade = Double.parseDouble(finalGrade);
            if (grade < 2 || grade > 6) {
                throw new InvalidFinalGradeException();
            } else {
                this.finalGrade = grade;
            }
        } else {
            //else it is either primary or not finished, so we will give it a value of 0
            this.finalGrade = grade;
        }


    }

    public String getInstitutionName() {
        return institutionName;
    }

    public LocalDate getEnrollmentDate() {
        return enrollmentDate;
    }

    public LocalDate getGraduationDate() {
        return graduationDate;
    }

    public String hasGraduated() {
        if (hasGraduated) {
            return "YES";
        }
        return "NO";
    }

    public double getFinalGrade() {
        return finalGrade;
    }

    protected void setEducationType(String educationType) {
        this.educationType = educationType;
    }

    public String getEducationType() {
        return educationType;
    }

}
