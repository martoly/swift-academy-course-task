package bg.swift.socialsystem.Exceptions;

public class BirthDateAfterTodayException extends Exception {
    public BirthDateAfterTodayException() {
        super("Birth date cannot be set in the future!");
    }
}
