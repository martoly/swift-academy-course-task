package bg.swift.socialsystem.Exceptions;

public class EmptyFieldException extends Exception {
    public EmptyFieldException() {
        super("Field cannot be empty!");
    }
}
