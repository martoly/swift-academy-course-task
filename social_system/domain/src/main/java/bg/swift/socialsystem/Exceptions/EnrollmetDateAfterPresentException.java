package bg.swift.socialsystem.Exceptions;

public class EnrollmetDateAfterPresentException extends Exception {
    public EnrollmetDateAfterPresentException() {
        super("Enrollment date cannot be set in the future!");
    }
}
