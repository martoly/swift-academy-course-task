package bg.swift.socialsystem.Exceptions;

public class GraduationDateBeforeEnrollmentDateException extends Exception {
    public GraduationDateBeforeEnrollmentDateException() {
        super("Graduation date cannot be set before the enrollment date!");
    }
}
