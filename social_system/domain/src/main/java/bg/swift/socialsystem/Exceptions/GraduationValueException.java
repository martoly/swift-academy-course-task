package bg.swift.socialsystem.Exceptions;

public class GraduationValueException extends Exception {
    public GraduationValueException() {
        super("Graduation value must be 'YES' or 'NO'!");
    }
}
