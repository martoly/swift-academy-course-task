package bg.swift.socialsystem.Exceptions;

public class HeightNegativeNumberOrZeroException extends Exception {
    public HeightNegativeNumberOrZeroException() {
        super("Height must be a positive number!");
    }
}
