package bg.swift.socialsystem.Exceptions;

public class InsuranceAmountNegativeException extends Exception {
    public InsuranceAmountNegativeException() {
        super("Social insuarance amount cannoot be a negative number!");
    }
}
