package bg.swift.socialsystem.Exceptions;

public class InvalidFinalGradeException extends Exception {
    public InvalidFinalGradeException() {
        super("Invalid value for final grade!");
    }
}
