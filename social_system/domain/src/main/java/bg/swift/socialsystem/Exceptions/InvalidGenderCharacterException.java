package bg.swift.socialsystem.Exceptions;

public class InvalidGenderCharacterException extends Exception {
    public InvalidGenderCharacterException() {
        super("Invalid gender character!");
    }
}
