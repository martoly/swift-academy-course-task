package bg.swift.socialsystem.Exceptions;

public class NullEducationException extends Exception {
    public NullEducationException() {
        super("Education was not created successfully!");
    }
}
