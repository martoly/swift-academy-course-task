package bg.swift.socialsystem.Exceptions;

public class SocialRecordAfterPresentException extends Exception {
    public SocialRecordAfterPresentException() {
        super("Social insurance record cannot be made in the future!");
    }
}
