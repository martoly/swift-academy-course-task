package bg.swift.socialsystem;

import bg.swift.socialsystem.Education.*;
import bg.swift.socialsystem.Exceptions.NullEducationException;
import bg.swift.socialsystem.Person.Address;
import bg.swift.socialsystem.Person.Person;
import bg.swift.socialsystem.SocialInsurance.SocialInsurance;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class InformationParser {

    public static Person personCreator(String[] line1, String[] line2, String[] line3) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyy");
        String firstName = line1[0];
        String lastName = line1[1];
        char gender = line1[2].charAt(0);
        int height = Integer.parseInt(line1[3]);
        LocalDate birthDate = LocalDate.parse(line1[4], formatter);
        String[] addressInfo = getAddress(line1);
        Address address = addressCreator(addressInfo);


        if (line2.length > 6 && line3.length > 3) {
            List<Education> educations = multipleEducations(line2);
            Set<SocialInsurance> socialInsurances = multipleSocialInsuranceRecords(line3);
            return new Person(firstName, lastName, gender, height, birthDate, address, educations, socialInsurances);
        } else if (line2.length > 6 && line3.length == 3) {
            List<Education> educations = multipleEducations(line2);
            SocialInsurance socialInsurance = socialInsuranceCreator(line3);
            return new Person(firstName, lastName, gender, height, birthDate, address, educations, socialInsurance);
        } else if (line2.length == 6 && line3.length > 3) {
            Education education = educationCreater(line2);
            Set<SocialInsurance> socialInsurances = multipleSocialInsuranceRecords(line3);
            return new Person(firstName, lastName, gender, height, birthDate, address, education, socialInsurances);
        } else {
            Education education = educationCreater(line2);
            SocialInsurance socialInsurance = socialInsuranceCreator(line3);
            return new Person(firstName, lastName, gender, height, birthDate, address, education, socialInsurance);
        }


    }

    private static List<Education> multipleEducations(String[] line2) throws Exception {
        String[] line2Copy = line2.clone();
        List<Education> educations = new LinkedList<>();
        for (int i = 0; i < line2.length / 6; i++) {
            educations.add(educationCreater(line2Copy));
            line2Copy = trimEducation(line2Copy);
        }
        return educations;
    }

    private static Set<SocialInsurance> multipleSocialInsuranceRecords(String[] line3) throws Exception {
        String[] line3Copy = line3.clone();
        Set<SocialInsurance> socialInsurances = new HashSet<>();
        for (int i = 0; i < line3.length / 6; i++) {
            socialInsurances.add(socialInsuranceCreator(line3Copy));
            line3Copy = trimSocialInsurance(line3Copy);
        }
        return socialInsurances;
    }

    private static SocialInsurance socialInsuranceCreator(String[] line3) throws Exception {
        double amount = Double.parseDouble(line3[0]);
        int year = Integer.parseInt(line3[1]);
        int month = Integer.parseInt(line3[2]);
        return new SocialInsurance(amount, year, month);
    }

    private static String[] trimSocialInsurance(String[] line) {
        String[] newLine = new String[line.length - 3];
        int l = 0;
        for (int i = 3; i < line.length; i++) {
            newLine[l] = line[i];
            l++;
        }
        return newLine;
    }

    private static String[] trimEducation(String[] line) {
        String[] newLine = new String[line.length - 6];
        int l = 0;
        for (int i = 6; i < line.length; i++) {
            newLine[l] = line[i];
            l++;
        }
        return newLine;
    }

    private static Education educationCreater(String[] input) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyy");
        String institutionName = input[1];
        LocalDate enrollmentDate = LocalDate.parse(input[2], formatter);
        LocalDate graduationDate = LocalDate.parse(input[3], formatter);
        String hasGraduated = input[4];
        String finalGrade = input[5];
        char educationType = input[0].toUpperCase().charAt(0);
        switch (educationType) {
            case 'P':
                return new PrimaryEducation(institutionName, enrollmentDate, graduationDate, hasGraduated, finalGrade);
            case 'S':
                return new SecondaryEducation(institutionName, enrollmentDate, graduationDate, hasGraduated, finalGrade);
            case 'B':
                return new BachelorEducation(institutionName, enrollmentDate, graduationDate, hasGraduated, finalGrade);
            case 'M':
                return new MasterEducation(institutionName, enrollmentDate, graduationDate, hasGraduated, finalGrade);
            case 'D':
                return new DoctorateEducation(institutionName, enrollmentDate, graduationDate, hasGraduated, finalGrade);
        }

        throw new NullEducationException();
    }

    private static String[] getAddress(String[] input) {
        //will user a for-cycle to get the address - which is the last 6 pieces of String on the first line :/
        String[] addressInfo = new String[6];
        int l = 0;
        for (int i = input.length - 6; i < input.length; i++) {
            addressInfo[l] = input[i];
            l++;
        }
        return addressInfo;
    }

    private static Address addressCreator(String[] input) throws Exception {
        return new Address(input[0], input[1], input[2], input[3], input[4], input[5]);
    }
}
