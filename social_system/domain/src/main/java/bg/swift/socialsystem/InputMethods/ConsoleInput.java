package bg.swift.socialsystem.InputMethods;

import java.util.Scanner;

public class ConsoleInput implements Input {
    @Override
    public String splitBy()
    {
        return ";";
    }

    @Override
    public String[] informationParser()
    {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine().split(splitBy());
    }

    @Override
    public int initialSizeParser()
    {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        return Integer.parseInt(string);
    }
}
