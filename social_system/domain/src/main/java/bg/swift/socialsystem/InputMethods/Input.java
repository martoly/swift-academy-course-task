package bg.swift.socialsystem.InputMethods;

public interface Input {
    String splitBy();

    String[] informationParser();

    int initialSizeParser();
}
