package bg.swift.socialsystem.InterfaceRepos;

import bg.swift.socialsystem.Person.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, Integer> {
    List<Person> findByFirstName(String firstName);
    List<Person> findByLastName(String lastName);
}
