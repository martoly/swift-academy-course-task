package bg.swift.socialsystem.Person;

import bg.swift.socialsystem.Exceptions.EmptyFieldException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String country;
    private String city;
    private String municipality;
    private String postalNumber;
    private String streetName;
    private String streetNumber;

    public Address(String country, String city, String municipality, String postalNumber, String streetName, String streetNumber) throws Exception {
        setCountry(country);
        setCity(city);
        setMunicipality(municipality);
        setPostalNumber(postalNumber);
        setStreetName(streetName);
        setStreetNumber(streetNumber);
    }

    private Address() {

    }

    private void setCountry(String country) throws Exception {
        if (country.equals("")) {
            throw new EmptyFieldException();
        }
        this.country = country;
    }

    private void setCity(String city) throws Exception {
        if (city.equals("")) {
            throw new EmptyFieldException();
        }
        this.city = city;
    }

    private void setMunicipality(String municipality) throws Exception {
        if (municipality.equals("")) {
            throw new EmptyFieldException();
        }
        this.municipality = municipality;
    }

    private void setPostalNumber(String postalNumber) throws Exception {
        if (postalNumber.equals("")) {
            throw new EmptyFieldException();
        }
        this.postalNumber = postalNumber;
    }

    private void setStreetName(String streetName) throws Exception {
        if (streetName.equals("")) {
            throw new EmptyFieldException();
        }
        this.streetName = streetName;
    }

    private void setStreetNumber(String streetNumber) throws Exception {
        if (streetNumber.equals("")) {
            throw new EmptyFieldException();
        }
        this.streetNumber = streetNumber;
    }

    public Integer getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getMunicipality() {
        return municipality;
    }

    public String getPostalNumber() {
        return postalNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    @Override
    public String toString() {
        return streetNumber + " " + streetName + " Street\n" +
                postalNumber + " " + municipality + "\n" +
                city + ", " + country;
    }

    public String[] print() {
        String line1 = String.format("%s %s Street", streetNumber, streetName);
        String line2 = String.format("%s %s", postalNumber, municipality);
        String line3 = String.format("%s, %s", city, country);
        String[] res ={line1, line2, line3};
        return res;
    }

}
