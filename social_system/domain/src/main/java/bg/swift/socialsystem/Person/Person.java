package bg.swift.socialsystem.Person;

import bg.swift.socialsystem.Education.Education;
import bg.swift.socialsystem.Exceptions.BirthDateAfterTodayException;
import bg.swift.socialsystem.Exceptions.EmptyFieldException;
import bg.swift.socialsystem.Exceptions.HeightNegativeNumberOrZeroException;
import bg.swift.socialsystem.Exceptions.InvalidGenderCharacterException;
import bg.swift.socialsystem.SocialInsurance.SocialInsurance;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;
    private String lastName;
    private char gender;
    private int height;
    private LocalDate birthDate;
    //???
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private SocialInsurance lastSocialInsuranceAdded;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Address address;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Education> educations = new LinkedList<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<SocialInsurance> socialInsurances = new HashSet<>();

    private Person() {
    }

    private Person(String firstName, String lastName, char gender, int height, LocalDate birthDate,
                   Address address) throws Exception {
        setFirstName(firstName);
        setLastName(lastName);
        setGender(gender);
        setHeight(height);
        setBirthDate(birthDate);
        setAddress(address);
    }

    public Person(String firstName, String lastName, char gender, int height, LocalDate birthDate,
                  Address address, Education education, SocialInsurance socialInsurance) throws Exception {
        this(firstName, lastName, gender, height, birthDate, address);
        setEducation(education);
        setSocialInsurance(socialInsurance);
    }

    public Person(String firstName, String lastName, char gender, int height, LocalDate birthDate,
                  Address address, Education education, Set<SocialInsurance> socialInsurances) throws Exception {
        this(firstName, lastName, gender, height, birthDate, address);
        setEducation(education);
        setSocialInsurances(socialInsurances);
    }

    public Person(String firstName, String lastName, char gender, int height, LocalDate birthDate,
                  Address address, List<Education> educations, SocialInsurance socialInsurance) throws Exception {
        this(firstName, lastName, gender, height, birthDate, address);
        setEducations(educations);
        setSocialInsurance(socialInsurance);
    }

    public Person(String firstName, String lastName, char gender, int height, LocalDate birthDate,
                  Address address, List<Education> educations, Set<SocialInsurance> socialInsurances) throws Exception {
        this(firstName, lastName, gender, height, birthDate, address);
        setEducations(educations);
        setSocialInsurances(socialInsurances);
    }

    private void setFirstName(String firstName) throws Exception {
        if (firstName.equals("")) {
            throw new EmptyFieldException();
        }
        this.firstName = firstName;
    }

    private void setLastName(String lastName) throws Exception {
        if (lastName.equals("")) {
            throw new EmptyFieldException();
        }
        this.lastName = lastName;
    }

    private void setGender(char gender) throws Exception {
        if (!(gender == 'm' || gender == 'f' || gender == 'M' || gender == 'F')) {
            throw new InvalidGenderCharacterException();
        }
        this.gender = gender;
    }

    private void setHeight(int height) throws Exception {
        if (height <= 0) {
            throw new HeightNegativeNumberOrZeroException();
        }
        this.height = height;
    }

    private void setBirthDate(LocalDate birthDate) throws Exception {
        if (birthDate.isAfter(LocalDate.now())) {
            throw new BirthDateAfterTodayException();
        }
        this.birthDate = birthDate;
    }

    private void setAddress(Address address) throws Exception {
        this.address = address;
    }

    private void setEducations(List<Education> educations) {
        this.educations.addAll(educations);
    }

    private void setEducation(Education education) {
        this.educations.add(education);
    }

    private void setSocialInsurance(SocialInsurance socialInsurance) {
        this.socialInsurances.add(socialInsurance);
        if (this.lastSocialInsuranceAdded.getDate().isBefore(socialInsurance.getDate())) {
            this.lastSocialInsuranceAdded = socialInsurance;
        }
    }

    private void setSocialInsurances(Set<SocialInsurance> socialInsurances) {
        this.socialInsurances.addAll(socialInsurances);
        for (SocialInsurance current : socialInsurances) {
            if (this.lastSocialInsuranceAdded == null) {
                this.lastSocialInsuranceAdded = current;
            } else if (this.lastSocialInsuranceAdded.getDate().isBefore(current.getDate())) {
                this.lastSocialInsuranceAdded = current;
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public char getGender() {
        return gender;
    }

    public int getHeight() {
        return height;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public Address getAddress() {
        return address;
    }

    public List<Education> getEducations() {
        return educations;
    }

    public Education getEducation() {
        return educations.get(educations.size() - 1);
    }

    public Set<SocialInsurance> getSocialInsurances() {
        return socialInsurances;
    }

    public SocialInsurance getSocialInsurance() {
        return lastSocialInsuranceAdded;
    }

    public void addEducations(List<Education> educations) {
        setEducations(educations);
    }

    public void addEducation(Education education) {
        setEducation(education);
    }

    public void addSocialInsuranceRecord(SocialInsurance socialInsurance) {
        this.socialInsurances.add(socialInsurance);
        if (this.lastSocialInsuranceAdded.getDate().isBefore(socialInsurance.getDate())) {
            this.lastSocialInsuranceAdded = socialInsurance;
        }
    }

    public void addSocialInsuranceRecords(Set<SocialInsurance> socialInsurances) {
        this.socialInsurances.addAll(socialInsurances);
        for (SocialInsurance current : socialInsurances) {
            if (this.lastSocialInsuranceAdded == null) {
                this.lastSocialInsuranceAdded = current;
            } else if (this.lastSocialInsuranceAdded.getDate().isBefore(current.getDate())) {
                this.lastSocialInsuranceAdded = current;
            }
        }
    }

}
