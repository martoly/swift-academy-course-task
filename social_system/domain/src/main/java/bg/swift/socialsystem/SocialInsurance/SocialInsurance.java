package bg.swift.socialsystem.SocialInsurance;

import bg.swift.socialsystem.Exceptions.InsuranceAmountNegativeException;
import bg.swift.socialsystem.Exceptions.SocialRecordAfterPresentException;
import net.bytebuddy.asm.Advice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class SocialInsurance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private double amount;
    private int year;
    private int month;

    private SocialInsurance() {
    }

    public SocialInsurance(double amount, int year, int month) throws Exception {
        setAmount(amount);
        setDate(year, month);
    }

    private void setAmount(double amount) throws Exception {
        if (amount < 0) {
            throw new InsuranceAmountNegativeException();
        }
        this.amount = amount;
    }

    private void setDate(int year, int month) throws Exception {
        if (LocalDate.now().getYear() < year || (LocalDate.now().getYear() == year && LocalDate.now().getMonthValue() < month)) {
            throw new SocialRecordAfterPresentException();
        }
        this.year = year;
        this.month = month;
    }

    public double getAmount() {
        return amount;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public LocalDate getDate() {
        return LocalDate.of(this.year, this.month, 1);

    }
}
