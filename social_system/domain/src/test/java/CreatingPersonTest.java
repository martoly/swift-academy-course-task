import bg.swift.socialsystem.Education.*;
import bg.swift.socialsystem.Person.Address;
import bg.swift.socialsystem.Person.Person;
import bg.swift.socialsystem.SocialInsurance.SocialInsurance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class CreatingPersonTest {
    @Test
    void test_input_with_list_of_educations_list_of_social_insurance_records() throws Exception {
        Address address = new Address("България", "София", "Триадица", "1010", "Георги Кондолов", "13");

        List<Education> educations = new LinkedList<>();
        educations.add(new PrimaryEducation("СОУ Гео Милев", LocalDate.of(1981, 9, 28), LocalDate.of(1985, 6, 30), "YES", ""));
        educations.add(new SecondaryEducation("СОУ Гео Милев", LocalDate.of(1987, 9, 23), LocalDate.of(1992, 5, 20), "YES", "5.165"));

        Set<SocialInsurance> socialInsurances = new HashSet<>();
        socialInsurances.add(new SocialInsurance(1606.60, 2018,10));
        socialInsurances.add(new SocialInsurance(1606.60, 2018,11));

        Person p = new Person("Емилия", "Младенова", 'F', 99, LocalDate.of(1975, 11, 2), address, educations, socialInsurances);

        Assertions.assertEquals(p.getFirstName(), "Емилия");
        Assertions.assertEquals(p.getLastName(), "Младенова");
        Assertions.assertEquals(p.getAddress(), address);
        Assertions.assertEquals(p.getBirthDate(), LocalDate.of(1975, 11, 2));
        Assertions.assertEquals(p.getGender(), 'F');
        Assertions.assertEquals(p.getHeight(), 99);
        Assertions.assertEquals(p.getEducations(), educations);
        Assertions.assertEquals(p.getSocialInsurances(), socialInsurances);
    }

    @Test
    void test_input_with_adding_list_of_educations_list_of_social_insurance_records() throws Exception {
        Address address = new Address("България", "София", "Триадица", "1010", "Георги Кондолов", "13");

        List<Education> educations1 = new LinkedList<>();
        educations1.add(new PrimaryEducation("СОУ Гео Милев", LocalDate.of(1981, 9, 28), LocalDate.of(1985, 6, 30), "YES", ""));
        educations1.add(new SecondaryEducation("СОУ Гео Милев", LocalDate.of(1987, 9, 23), LocalDate.of(1992, 5, 20), "YES", "5.165"));
        Set<SocialInsurance> socialInsurances = new HashSet<>();
        socialInsurances.add(new SocialInsurance(1606.60,2018,3));
        socialInsurances.add(new SocialInsurance(1606.60,2018,4));
        Person p = new Person("Емилия", "Младенова", 'F', 99, LocalDate.of(1975, 11, 2), address, educations1, socialInsurances);

        List<Education> educations2 = new LinkedList<>();
        educations2.add(new DoctorateEducation("СОУ Климет Охридски", LocalDate.of(1981, 9, 28), LocalDate.of(1985, 6, 30), "YES", ""));
        educations2.add(new BachelorEducation("СУ Васил Левски", LocalDate.of(1987, 9, 23), LocalDate.of(1992, 5, 20), "YES", "5.165"));

        Set<SocialInsurance> socialInsurances2 = new HashSet<>();
        socialInsurances2.add(new SocialInsurance(1606.60,2015,3));
        socialInsurances2.add(new SocialInsurance(1606.60,2015,4));

        List<Education> tempList = new LinkedList<>();
        tempList.addAll(educations1);
        tempList.addAll(educations2);
        Set<SocialInsurance> tempSocial = new HashSet<>();
        tempSocial.addAll(socialInsurances);
        tempSocial.addAll(socialInsurances2);
        Assertions.assertEquals(p.getFirstName(), "Емилия");
        Assertions.assertEquals(p.getLastName(), "Младенова");
        Assertions.assertEquals(p.getAddress(), address);
        Assertions.assertEquals(p.getBirthDate(), LocalDate.of(1975, 11, 2));
        Assertions.assertEquals(p.getGender(), 'F');
        Assertions.assertEquals(p.getHeight(), 99);
        Assertions.assertEquals(p.getEducations(), educations1);
        p.addEducations(educations2);
        Assertions.assertEquals(p.getEducations(), tempList);
        Assertions.assertEquals(p.getSocialInsurances(), socialInsurances);
        p.addSocialInsuranceRecords(socialInsurances2);
        Assertions.assertEquals(p.getSocialInsurances(), tempSocial);
    }

    @Test
    void test_input_with_single_education_single_social_insurance_record() throws Exception {
        Address address = new Address("България", "София", "Триадица", "1010", "Георги Кондолов", "13");
        Education education = new PrimaryEducation("СОУ Гео Милев", LocalDate.of(1981, 9, 28), LocalDate.of(1985, 6, 30), "YES", "");
        SocialInsurance socialInsurance = new SocialInsurance(1606.60, 2018,5);
        Person p = new Person("Емилия", "Младенова", 'F', 99, LocalDate.of(1975, 11, 2), address, education, socialInsurance);

        Assertions.assertEquals(p.getFirstName(), "Емилия");
        Assertions.assertEquals(p.getLastName(), "Младенова");
        Assertions.assertEquals(p.getAddress(), address);
        Assertions.assertEquals(p.getBirthDate(), LocalDate.of(1975, 11, 2));
        Assertions.assertEquals(p.getGender(), 'F');
        Assertions.assertEquals(p.getHeight(), 99);
        Assertions.assertEquals(p.getEducation(), education);
        Assertions.assertEquals(p.getSocialInsurance(), socialInsurance);
    }

    @Test
    void test_input_with_adding_single_education_single_social_insurance_record() throws Exception {
        Address address = new Address("България", "София", "Триадица", "1010", "Георги Кондолов", "13");
        Education education1 = new PrimaryEducation("СОУ Гео Милев", LocalDate.of(1981, 9, 28), LocalDate.of(1985, 6, 30), "YES", "");
        Education education2 = new SecondaryEducation("СУ Васил Левски", LocalDate.of(1981, 9, 28), LocalDate.of(1985, 6, 30), "YES", "5.6");
        SocialInsurance socialInsurance1 = new SocialInsurance(1606.60,2017,2);
        SocialInsurance socialInsurance2 = new SocialInsurance(1606.60,2018,3);
        Person p = new Person("Емилия", "Младенова", 'F', 99, LocalDate.of(1975, 11, 2), address, education1, socialInsurance1);

        List<Education> tempList = new LinkedList<>();
        tempList.add(education1);
        tempList.add(education2);
        Set<SocialInsurance> tempSocial = new HashSet<>();
        tempSocial.add(socialInsurance1);
        tempSocial.add(socialInsurance2);
        Assertions.assertEquals(p.getFirstName(), "Емилия");
        Assertions.assertEquals(p.getLastName(), "Младенова");
        Assertions.assertEquals(p.getAddress(), address);
        Assertions.assertEquals(p.getBirthDate(), LocalDate.of(1975, 11, 2));
        Assertions.assertEquals(p.getGender(), 'F');
        Assertions.assertEquals(p.getHeight(), 99);
        Assertions.assertEquals(p.getEducation(), education1);
        Assertions.assertEquals(p.getEducation().getFinalGrade(), 0);
        Assertions.assertEquals(p.getSocialInsurance(), socialInsurance1);
        p.addSocialInsuranceRecord(socialInsurance2);
        p.addEducation(education2);
        Assertions.assertEquals(p.getEducation(), education2);
        Assertions.assertEquals(p.getEducation().getFinalGrade(), 5.6);
        Assertions.assertEquals(p.getSocialInsurance(), socialInsurance2);
        Assertions.assertEquals(p.getEducations(), tempList);
        Assertions.assertEquals(p.getSocialInsurances(), tempSocial);
    }
}
