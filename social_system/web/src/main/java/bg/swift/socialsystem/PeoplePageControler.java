package bg.swift.socialsystem;

import bg.swift.socialsystem.InterfaceRepos.PersonRepository;
import bg.swift.socialsystem.Person.Person;
import bg.swift.socialsystem.SocialInsurance.SocialInsurance;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static bg.swift.socialsystem.InformationParser.personCreator;


@Controller
public class PeoplePageControler {

    private PersonRepository personRepository;

    public PeoplePageControler(PersonRepository personRepository) throws Exception {
        this.personRepository = personRepository;
        initialize();
    }

    @GetMapping("/people")
    public String peoplePage(Model model,
                             @RequestParam(value = "firstName", required = false) String firstName,
                             @RequestParam(value = "lastName", required = false) String lastName) {
        model.addAttribute("spacer", " ");
        Set<Person> toBeDisplayed = new HashSet<>();
        if (firstName != null && lastName != null) {
            toBeDisplayed.addAll(personRepository.findByFirstName(firstName));
            toBeDisplayed.addAll(personRepository.findByLastName(lastName));
        } else if (firstName == null && lastName != null) {
            toBeDisplayed.addAll(personRepository.findByLastName(lastName));
        } else if (firstName != null && lastName == null) {
            toBeDisplayed.addAll(personRepository.findByFirstName(firstName));
        } else {
            model.addAttribute("people", personRepository.findAll());
            return "people";
        }
        model.addAttribute("people", toBeDisplayed);
        return "people";
    }

    @GetMapping("/people/{id}")
    public String getPersonBy(@PathVariable("id") Integer id,
                              Model model) {
        Person person = personRepository.findById(id).get();
        model.addAttribute("person", person);
        model.addAttribute("spacer", " ");

        model.addAttribute("socialRecords", person.getSocialInsurances());
        model.addAttribute("addressString", person.getAddress());

        return "personDetails";
    }

    @GetMapping("/isEligible/{id}")
    public String isEligible(@PathVariable("id") Integer id,
                             Model model) {
        Person person = personRepository.findById(id).get();
        model.addAttribute("spacer", " ");
        model.addAttribute("person", person);
        model.addAttribute("socialRecords", person.getSocialInsurances());
        model.addAttribute("addressString", person.getAddress());

        LocalDate lastSocialRecordDate = person.getSocialInsurance().getDate();
        boolean socialRecordBeforeThreeMonths = LocalDate.now().minusMonths(3).isBefore(lastSocialRecordDate);
        double allSocialRecordsAmount = 0;
        int i = 0;
        for (SocialInsurance current : person.getSocialInsurances()) {
            if (i < 27) {
                allSocialRecordsAmount += current.getAmount();
                i++;
            } else {
                break;
            }
        }

        if (!person.getEducation().getEducationType().equals("P") && socialRecordBeforeThreeMonths) {
            double amount = allSocialRecordsAmount / 24;
            String result = String.format("%.2f",amount);
            model.addAttribute("getAmount", result);
            return "personDetailsEligible";
        } else {
            return "personDetailsNotEligible";
        }
    }

    private void initialize() throws Exception {
        String[] line1 = "Емилия;Младенова;F;99;2.11.1975;България;София;Триадица;1010;Георги Кондолов;13".split(";");
        String[] line2 = "P;СОУ Гео Милев;28.9.1981;30.6.1985;YES;;S;СОУ Гео Милев;23.9.1987;20.5.1992;YES;5.165".split(";");
        String[] line3 = "1606.60;2018;10;1060.78;2018;9;779.25;2018;8;1356.33;2018;6;1924.28;2018;5;862.18;2018;4;1767.59;2018;3".split(";");
        personRepository.save(personCreator(line1, line2, line3));

        line1 = "Мартин;Любенов;M;184;4.07.1998;България;Севлиево;Триадица;5400;Тридесета;11".split(";");
        line2 = "P;СУ Васил Левски;15.9.2005;30.6.2009;YES;;S;СУ Васил Левски;15.9.2009;30.6.2012;YES;5.85".split(";");
        line3 = "400.60;2017;10;400.78;2017;9".split(";");
        personRepository.save(personCreator(line1, line2, line3));

        line1 = "Енис;Янев;F;174;4.10.1998;България;Хасково;Столипиново;5400;Тридесета;69".split(";");
        line2 = "P;СУ Стоян Колев;15.9.2005;30.6.2010;YES;;S;СУ Динко Вълев;15.9.2010;30.6.2015;YES;3.85".split(";");
        line3 = "400.60;2017;10;2300.78;2017;9".split(";");
        personRepository.save(personCreator(line1, line2, line3));

        line1 = "Георги;Матев;M;184;25.05.1998;България;Севлиево;Гочо Москов;5400;Тридесета;17".split(";");
        line2 = "P;СУ Първо Основно;15.9.2005;30.6.2009;YES;;S;СУ Васил Левски;15.9.2009;30.6.2012;YES;4.85".split(";");
        line3 = "400.60;2017;10;400.78;2017;9".split(";");
        personRepository.save(personCreator(line1, line2, line3));


        line1 = "Георги;Тодоров;M;174;07.05.1998;България;Севлиево;Триадица;5400;Кирцата И Мето;17".split(";");
        line2 = "P;СУ Васил Левски;15.9.2005;30.6.2009;YES;;S;СУ Васил Левски;15.9.2009;30.6.2012;YES;5.85".split(";");
        line3 = "400.60;2017;10;400.78;2017;9".split(";");
        personRepository.save(personCreator(line1, line2, line3));


        line1 = "Мартин;Любенов;M;184;4.07.1998;България;Севлиево;Триадица;5400;Тридесета;11".split(";");
        line2 = "P;СУ Васил Левски;15.9.2005;30.6.2009;YES;;S;СУ Васил Левски;15.9.2009;30.6.2012;YES;5.85".split(";");
        line3 = "400.60;2017;10;400.78;2017;9".split(";");
        personRepository.save(personCreator(line1, line2, line3));

        line1 = "Мартин;Любенов;M;184;4.07.1998;България;Севлиево;Триадица;5400;Тридесета;11".split(";");
        line2 = "P;СУ Васил Левски;15.9.2005;30.6.2009;YES;;S;СУ Васил Левски;15.9.2009;30.6.2012;YES;5.85".split(";");
        line3 = "400.60;2017;10;400.78;2017;9".split(";");
        personRepository.save(personCreator(line1, line2, line3));


    }

}
